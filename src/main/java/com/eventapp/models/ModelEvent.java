package com.eventapp.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ModelEvent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	public String nome;
	public String local;
	public String data;
	public String hora;
	
	@OneToMany
	private List<ModelGuests> guests;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;

	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;

	}

	public List<ModelGuests> getGuests() {
		return guests;
	}

	public void setGuests(List<ModelGuests> guests) {
		this.guests = guests;
	}
}
