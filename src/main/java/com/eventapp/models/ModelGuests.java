package com.eventapp.models;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;



@Entity
public class ModelGuests {
	
	@Id	
	private String rg;
	private String nomeConvidado;
	
	@ManyToOne
	private ModelEvent event;
	
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getNomeConvidado() {
		return nomeConvidado;
	}
	public void setNomeConvidado(String nomeConvidado) {
		this.nomeConvidado = nomeConvidado;
	}
	
	public ModelEvent getEvent() {
		return event;
	}
	public void setEvent(ModelEvent event) {
		this.event = event;
	}	
}
