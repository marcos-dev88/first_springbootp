package com.eventapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.eventapp.models.ModelEvent;

public interface RepositoryEvent extends CrudRepository<ModelEvent, String> {

	ModelEvent findById(long id);
}

