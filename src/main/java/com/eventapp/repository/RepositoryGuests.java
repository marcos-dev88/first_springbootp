package com.eventapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.eventapp.models.ModelGuests;

public interface RepositoryGuests extends CrudRepository<ModelGuests, String> {
	
	//Consulta personalizada ao DataBase
	@Query("SELECT g FROM ModelGuests g WHERE event_id = ?1")
    List<ModelGuests> getGuests(long id);
	
	ModelGuests findByRg(String rg);
}
