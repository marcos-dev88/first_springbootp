package com.eventapp.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.eventapp.models.ModelEvent;
import com.eventapp.models.ModelGuests;
import com.eventapp.repository.RepositoryEvent;
import com.eventapp.repository.RepositoryGuests;

@Controller
public class ControllerEvent {
	
	@Autowired
	private RepositoryEvent re;
	
	@Autowired
	private RepositoryGuests rgu;
	
	@GetMapping(value="/cadastrarEvento")
	public String form() {
		return "../event/formEvent";
	}

	@PostMapping(value="/cadastrarEvento")
	public String form(ModelEvent event) {
		
		re.save(event);
		
		return "redirect:/cadastrarEvento";
	}
	
	@RequestMapping(value="/deleteEvent")
	public String deleteEvent(Long id) {
		ModelEvent event = re.findById(id);
		re.delete(event);
		return "redirect:/eventos";
	}
	
	@RequestMapping(value="/deleteGuest")
	public String deleteGuest(String rg) {
		ModelGuests mg = rgu.findByRg(rg);
		rgu.delete(mg);
		
		ModelEvent event = mg.getEvent();
		Long idL = event.getId();
		String id = "" + idL;
		return "redirect:/" + id;
	}
	
	
	@GetMapping(value="/")
	public String voltarInicio() {
		return "../index";
	}
	
	@RequestMapping("/eventos")
	public ModelAndView listEvents() {
		ModelAndView mav = new ModelAndView("../event/eventRegister");
		Iterable<ModelEvent> event = re.findAll();
		mav.addObject("event", event);
		return mav;
	}
	
	@GetMapping(value="/{id}")
	public ModelAndView detalhesEvent(@PathVariable("id") long id){
		ModelEvent event = re.findById(id);
		ModelAndView mav = new ModelAndView("../event/detalhesEvento");
		mav.addObject("event", event);
		
		List<ModelGuests> guests = rgu.getGuests(id);
		mav.addObject("guests", guests);
		return mav;		
	}
	
 	@PostMapping(value="/{id}")
	public String detalhesEventPost(@PathVariable("id")long id, ModelGuests guests) {
		ModelEvent event = re.findById(id);
		guests.setEvent(event);
		rgu.save(guests);
		return "redirect:/{id}"; 
	} 
 	
 	
}
