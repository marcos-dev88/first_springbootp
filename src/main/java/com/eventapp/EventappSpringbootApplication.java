package com.eventapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventappSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventappSpringbootApplication.class, args); 
	}

}
